<?php

namespace noname_emanon\cdek;

use Yii;

/**
 * Application Asset File.
 */
class Asset extends \yii\web\AssetBundle
{
    const ASSETS_PATH = '@vendor/noname_emanon/cdek-api/widget';

    public function init()
    {
        parent::init();

        [$path, $webPath] = self::getPublishedAssets();

        $this->js[] = $webPath . '/widjet.js';
    }

    protected static function getPublishedAssets()
    {
        return Yii::$app->getAssetManager()->publish(Yii::getAlias(self::ASSETS_PATH));
    }

    public static function getScriptsUrl()
    {
        [$path, $webPath] = self::getPublishedAssets();

        return $webPath . '/scripts/';
    }

    public static function getScriptsPath()
    {
        [$path, $webPath] = self::getPublishedAssets();

        return $path . '/scripts/';
    }
}
